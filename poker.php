<html>
<head>
<title>Michael Kohn's Poker</title>
<style>
<!--
  BODY { font-family:helvetica; }
  TD { font-family:helvetica;  font-size:12; }
  B { font-family:helvetica;  font-size:12; }
  P { font-family:helvetica;  font-size:12; }
  P.rcard { font-family:helvetica;  font-size:12; font-weight:bold; color:red; }
  P.bcard { font-family:helvetica;  font-size:12; font-weight:bold; color:black; }
-->
</style>
</head>
<body onload="init();">
<script type="text/javascript" src="Deck.js"></script>
<script type="text/javascript" src="Hand.js"></script>
<script type="text/javascript" src="Player.js"></script>
<script type="text/javascript" src="PlayerAI.js"></script>
<script type="text/javascript" src="Pot.js"></script>
<script type="text/javascript">

var deck = new Deck();
var player_ai = new PlayerAI();
var player_user = new Player();
var pot = new Pot();
var deal_count;
var show_count;
var clear_count;
var game_state = 0;
var game_count = 0;
var is_betting = false;
var is_raise = false;

var opponent = window.location.href.split("?")[1].split("=")[1]
//var state = 0;

function init()
{
/*
  filename = "player/" + opponent + "_" + state + ".jpeg";

  //console.log(filename);

  opponent = document.getElementById("opponent");
  opponent.innerHTML = "<img src='" + filename+ "'>";
*/

  setImage();

  // Bug in Firefox?
  document.getElementById("call").disabled = true;

  setBetButtons(4);
  updatePot();
}

function doSuggestion()
{
  let bits = player_user.hand.suggest();

  for (n = 0; n < 5; n++)
  {
    hold = document.getElementById("hold" + n);

    if ((bits & (1 << n)) == 0)
    {
      hold.innerHTML = "---";
    }
    else
    {
      hold.innerHTML = "<b>HOLD</b>";
    }
  }
}

function setMessageAI(message)
{
  message_ai = document.getElementById("message_ai");
  message_ai.innerHTML = "<font color='red'>" + message + "</font>";
}

function setMessageUser(message)
{
  message_user = document.getElementById("message_user");
  message_user.innerHTML = "<font color='red'>" + message + "</font>";
}

function updatePot()
{
  current_pot = document.getElementById("pot");
  money_user = document.getElementById("money_user");
  money_ai = document.getElementById("money_ai");

  current_pot.innerHTML = "Current Pot: $" + pot.getMoney();
  money_ai.innerHTML = "Money: $" + player_ai.getMoney();
  money_user.innerHTML = "Money: $" + player_user.getMoney();
}

function setBetButtons(bet_minimum)
{
  button_bet1 = document.getElementById("bet1");
  button_bet2 = document.getElementById("bet2");
  button_bet3 = document.getElementById("bet3");
  button_fold = document.getElementById("fold");

  button_bet1.disabled = bet_minimum > 1;
  button_bet2.disabled = bet_minimum > 2;
  button_bet3.disabled = bet_minimum > 3;
  button_fold.disabled = bet_minimum == 4;

}

function betAIFirst(value)
{
  if (value > player_ai.getCurrentBet())
  {
    let raise = player_ai.raise(value);

    if (raise == 0)
    {
      setMessageAI("I fold");
      game_state = 3;

      player_user.addMoney(pot.getMoney());
      pot.empty();

      //document.getElementById("deal").disabled = false;
      //document.getElementById("call").disabled = true;

      endGame();

      return;
    }
    else
    {
      setMessageAI("I'll match your bet");
      pot.addMoney(value);
    }
  }

  setBetButtons(4);
  is_betting = false;

  if (game_state == 1)
  {
    setMessageUser("Select cards to remove and press Deal");
    //document.getElementById("deal").disabled = false;
    //document.getElementById("call").disabled = true;
  }

  document.getElementById("deal").disabled = game_state == 3;
  document.getElementById("call").disabled = game_state != 3;
}

function betUserFirst(value)
{
  if (is_raise)
  {
    is_raise = false;
    is_betting = false;

    player_user.betMoney(value);
    pot.addMoney(value);

    setBetButtons(4);
    document.getElementById("deal").disabled = false;

    setMessageUser("Select cards to remove and press Deal");

    return;
  }

  let money = player_ai.computeBet();

  if (money < value)
  {
    if (player_ai.shouldFold(value))
    {
      setMessageAI("I fold");
      game_state = 3;

      player_user.addMoney(pot.getMoney());
      pot.empty();

      //document.getElementById("deal").disabled = false;
      ////document.getElementById("call").disabled = true;

      endGame();

      return;
    }
  }

  pot.addMoney(value);
  player_user.betMoney(value);

  if (money < value) { money = value; }

  pot.addMoney(money);
  player_ai.betMoney(money);

  if (money == value)
  {
    setMessageAI("I'll match that");
    setBetButtons(4);

    if (game_state == 1)
    {
      document.getElementById("deal").disabled = false;
      document.getElementById("call").disabled = true;
    }
    else
    {
      document.getElementById("deal").disabled = true;
      document.getElementById("call").disabled = false;
    }

    setMessageUser("Select cards to remove and press Deal");
    is_betting = false;
  }
  else
  {
    let diff = (money - value);
    setMessageAI("I'll raise " + diff);
    setBetButtons(4);

    document.getElementById("bet1").disabled = (diff != 1);
    document.getElementById("bet2").disabled = (diff != 2);
    document.getElementById("bet3").disabled = (diff != 3);

    is_raise = true;
  }
}

function bet(value)
{
  player_user.betMoney(value);
  pot.addMoney(value);

  if ((game_count & 1) == 1)
  {
    betAIFirst(value);
  }
  else
  {
    betUserFirst(value);
  }

  updatePot();
}

function newGame()
{
  deck.shuffle();

  //pot.empty();

  player_ai.betMoney(1);
  player_user.betMoney(1);
  pot.addMoney(2);

  updatePot();

  document.getElementById("deal").disabled = true;
  document.getElementById("call").disabled = true;
  setMessageUser("");
  setMessageAI("");

  for (n = 0; n < 5; n++)
  {
    player_ai.hand.setCard(n, deck.getCard());
    player_user.hand.setCard(n, deck.getCard());
  }

  deal_count = 0;
  is_betting = true;
}

function deal()
{
  let n, i;

  setMessageAI("");
  setMessageUser("");

  if (game_state == 0)
  {
    newGame();

    game_state++;
    game_count++;
    window.setTimeout(dealFirst, 100);
  }
  else if (game_state == 1)
  {
    let bits;

    bits = player_ai.hand.suggest();

    player_ai.hand.remove(bits);

    bits = 0;

    for (n = 0; n < 5; n++)
    {
      if (player_user.hand.isFlipped(n)) { bits |= (1 << n); }
    }

    player_user.hand.remove(bits);

    game_state++;

    window.setTimeout(dealSecond, 100);
  }
  else if (game_state == 3)
  {
    clear_count = 0;
    game_state = 1;
    game_count++;

    window.setTimeout(clearTable, 100);
  }
}

function call()
{
  show_count = 0;
  window.setTimeout(show, 200);
}

function fold()
{
  player_ai.addMoney(pot.getMoney());
  pot.empty();
  updatePot();

  setMessageAI("I win!");
  setMessageUser("You fold!");

  setBetButtons(4);

  document.getElementById("deal").disabled = false;

  game_state = 3;
}

function dealFirst()
{
  if (deal_count < 10)
  {
    let n = Math.floor(deal_count / 2);
    let i;

    if ((deal_count & 1) == 1)
    {
      card = document.getElementById("ai" + n);
      i = player_ai.hand.getCard(n);
      card.src = "cards/card_back.jpeg";
    }
    else
    {
      card = document.getElementById("user" + n);
      i = player_user.hand.getCard(n);
      card.src = "cards/card_back.jpeg";
    }
  }
  else
  {
    let n = deal_count - 10;
    card = document.getElementById("user" + n);
    i = player_user.hand.getCard(n);
    card.src = deck.getCardFilename(i);
  }

  deal_count++;

  if (deal_count != 15)
  {
    window.setTimeout(dealFirst, 200);
  }
  else
  {
    if ((game_count & 1) == 1)
    {
      money = player_ai.computeBet();
      player_ai.betMoney(money);
      pot.addMoney(money);
      updatePot();

      setBetButtons(money);

      setMessageAI("I will bet $" + money);
    }
    else
    {
      setMessageUser("Your turn to bet");
      setBetButtons(0);
    }
  }
}

function dealSecond()
{
  let i;

  while(true)
  {
    i = player_ai.hand.getNextRemove() 

    if (i != -1)
    {
      card = document.getElementById("ai" + i);
      i = player_ai.hand.getCard(i);
      card.src = "cards/card_blank.jpeg";

      break;
    }

    i = player_user.hand.getNextRemove() 

    if (i != -1)
    {
      card = document.getElementById("user" + i);
      i = player_user.hand.getCard(i);
      card.src = "cards/card_blank.jpeg";

      break;
    }

    i = player_ai.hand.getNextAdd() 

    if (i != -1)
    {
      card = document.getElementById("ai" + i);
      player_ai.hand.setCard(i, deck.getCard());
      i = player_ai.hand.getCard(i);
      card.src = "cards/card_back.jpeg";

      break;
    }

    i = player_user.hand.getNextAdd() 

    if (i != -1)
    {
      card = document.getElementById("user" + i);
      player_user.hand.setCard(i, deck.getCard());
      i = player_user.hand.getCard(i);
      card.src = deck.getCardFilename(i);

      break;
    }

    game_state++;

    document.getElementById("deal").disabled = true;
    document.getElementById("call").disabled = false;

    if ((game_count & 1) == 1)
    {
      money = player_ai.computeBet();
      player_ai.betMoney(money);
      pot.addMoney(money);
      updatePot();

      setBetButtons(money);

      setMessageAI("I will bet $" + money);

      document.getElementById("call").disabled = true;
    }
    else
    {
      setMessageUser("Your turn to bet");
      setBetButtons(0);
    }

    return;
  }

  window.setTimeout(dealSecond, 200);
}

function setImage()
{
  let state = player_ai.getState();
  let filename = "player/" + opponent + "_" + state + ".jpeg";

  let opponent_image = document.getElementById("opponent");
  opponent_image.innerHTML = "<img src='" + filename+ "'>";
}

function endGame()
{
  document.getElementById("deal").disabled = false;
  document.getElementById("call").disabled = true;

  setBetButtons(4);

  if (player_ai.getMoney() > 10)
  {
    let state = player_ai.getState();
    player_ai.buyBack();

    if (state != player_ai.getState())
    {
      setImage();
    }
  }

  if (player_user.getMoney() > 10)
  {
    player_user.buyBack();
  }

  if (player_user.checkOwe())
  {
    //console.log("player_user.checkOwe()");

    setMessageUser("You have lost your " + player_user.getState());

    window.setTimeout(endGame, 1000);
    return;
  }

  if (player_ai.checkOwe())
  {
    //console.log("player_user.checkOwe()");

    setMessageAI("I have lost my " + player_user.getState());
    setImage();
    updatePot();

    window.setTimeout(endGame, 1000);
    return;
  }

  document.getElementById("deal").disabled = false;
}

function show()
{
  let n;
  let value_ai;
  let value_user;

  if (show_count < 5)
  {
    card = document.getElementById("ai" + show_count);
    i = player_ai.hand.getCard(show_count);
    card.src = deck.getCardFilename(i);

    show_count++;
    window.setTimeout(show, 200);

    return;
  }

  value_ai = player_ai.hand.getValue();
  value_user = player_user.hand.getValue();

  hand_ai = player_ai.hand.getValueAsString(value_ai);
  hand_user = player_user.hand.getValueAsString(value_user);

  if (value_ai == value_user)
  {
    hand_ai += " - It's a draw";
    hand_user += " - It's a draw";
  }
  else if (value_ai < value_user)
  {
    hand_user += " - You win!";

    player_user.addMoney(pot.getMoney());
    pot.empty();
  }
  else
  {
    hand_ai += " - I win!";

    player_ai.addMoney(pot.getMoney());
    pot.empty();
  }

  updatePot();

  setMessageAI(hand_ai);
  setMessageUser(hand_user);

  document.getElementById("deal").disabled = true;
  document.getElementById("call").disabled = true;

  window.setTimeout(endGame, 1000);
}

function clearTable()
{
  if (clear_count < 5)
  {
    card = document.getElementById("ai" + clear_count);
    i = player_ai.hand.getCard(i);
    card.src = "cards/card_blank.jpeg";
  }
  else if (clear_count < 10)
  {
    card = document.getElementById("user" + (clear_count - 5));
    i = player_user.hand.getCard(i);
    card.src = "cards/card_blank.jpeg";
  }
  else
  {
    newGame();
    deal_count = 0;

    window.setTimeout(dealFirst, 100);
    return;
  }

  clear_count++;
  window.setTimeout(clearTable, 100);
}

function flip(index)
{
  let i;

  if (game_state != 1) { return; }
  if (is_betting) { return; }

  card = document.getElementById("user" + index);
  player_user.hand.flip(index);
  i = player_user.hand.getCard(index);
  card.src = deck.getCardFilename(i);
}

</script>

<div id="opponent"></div>

<table>
<tr>
<td><div id="money_ai">Money: $0</div></td>
<td><div id="message_ai"></div></td>
</tr>
</table>

<table>
<tr>
<td><img src="cards/card_blank.jpeg" id="ai0"></td>
<td><img src="cards/card_blank.jpeg" id="ai1"></td>
<td><img src="cards/card_blank.jpeg" id="ai2"></td>
<td><img src="cards/card_blank.jpeg" id="ai3"></td>
<td><img src="cards/card_blank.jpeg" id="ai4"></td>
</tr>
<tr>
<td colspan=5><hr></td>
</tr>
<tr>
<td colspan=5 id="pot">Current Pot: $0</td>
</tr>
<tr>
<td colspan=5><hr></td>
</tr>
<!-- tr>
<td id="hold0" align="center">/td>
<td id="hold1" align="center">/td>
<td id="hold2" align="center"></td>
<td id="hold3" align="center"></td>
<td id="hold4" align="center"></td>
</tr -->
<tr>
<td><img src="cards/card_blank.jpeg" id="user0" onclick="flip(0);"></td>
<td><img src="cards/card_blank.jpeg" id="user1" onclick="flip(1);"></td>
<td><img src="cards/card_blank.jpeg" id="user2" onclick="flip(2);"></td>
<td><img src="cards/card_blank.jpeg" id="user3" onclick="flip(3);"></td>
<td><img src="cards/card_blank.jpeg" id="user4" onclick="flip(4);"></td>
</tr>
</table>

<table>
<tr>
<td><div id="money_user">Money: $0</div></td>
<td colspan=4><div id="message_user"></div></td>
</tr>
</table>

<!-- div style="width:50; height:300; position:relative;">
</div -->
<button onclick="deal();" id="deal">Deal</button>
<button onclick="bet(1);" id="bet1" disabled=true>Bet 1</button>
<button onclick="bet(2);" id="bet2" disabled=true>Bet 2</button>
<button onclick="bet(3);" id="bet3" disabled=true>Bet 3</button>
<button onclick="fold();" id="fold" disabled=true>Fold</button>
<button onclick="call();" id="call" disabled=true>Call</button>
</body>
</html>

