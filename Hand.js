/**
 *  Poker Game
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2017 by Michael Kohn
 *
 */

function compare(a, b)
{
  return a - b;
}

function compare_r(a, b)
{
  return b - a;
}

class Hand
{
  constructor()
  {
    this.cards = new Array();
    this.showing = new Array();
    this.sorted = new Array();
    this.sorted_map = new Array();
    this.remove_list = new Array();
    this.add_list = new Array();
  }

  setCard(index, value)
  {
    this.cards[index] = value;
    this.showing[index] = true;
  }

  getCard(index)
  {
    if (this.showing[index] == false) { return -1; }

    return this.cards[index];
  }

  flip(index)
  {
    this.showing[index] = this.showing[index] == true ? false : true;
  }

  isFlipped(index)
  {
    return this.showing[index] == false;
  }

  optimize()
  {
    for (let n = 0; n < 5; n++)
    {
      this.sorted[n] = this.cards[n];
    }

    this.sorted.sort(compare);
  }

  getStraight()
  {
    if ((this.sorted[0] % 13) + 4 != (this.sorted[4] % 13))
    {
      return -1;
    }

/*
    for (let n = 0; n < 4; n++)
    {
      if ((this.sorted[n] % 13) != (this.sorted[n + 1] % 13) + 1) { return -1; }
    }
*/

    return this.sorted[0] % 13;
  }

  isFlush()
  {
    let value = Math.floor(this.cards[0] / 13);

    for (let n = 1; n < 5; n++)
    {
      if (value != Math.floor(this.cards[n] / 13)) { return false; }
    }

    return true;
  }

  getValue()
  {
    let straight = this.getStraight();
    let is_flush = this.isFlush();
    var counts = new Array();
    let n;

    // Create a sorted array of the cards.
    this.optimize();

    if (is_flush)
    {
      if (straight != -1)
      {
        if (straight == 9) { return 9; }

        return 8;
      }

      return 5;
    }

    for (n = 0; n < 13; n++)
    {
      counts[n] = 0;
    }

    for (n = 0; n < 5; n++)
    {
      counts[this.cards[n] % 13]++;
    }

    counts.sort(compare_r);

    if (counts[0] == 4) { return  7; }

    if (counts[0] == 3)
    {
      if (counts[1] == 2) { return 6; }

      return 3;
    }

    if (counts[0] == 2)
    {
      if (counts[1] == 2) { return 2; }

      return 1;
    }

    return 0;
  }

  getValueAsString(value)
  {
    var values_string = [
      "Nothing",          // 0
      "Pair",             // 1
      "Two Pair",         // 2
      "Three Of A Kind",  // 3
      "Straight",         // 4
      "Flush",            // 5
      "Full House",       // 6
      "Four Of A Kind",   // 7
      "Straight Flush",   // 8
      "Royal Flush"       // 9
    ];

    return values_string[value];
  }

  suggest()
  {
    var almost_flush = new Array();
    var pairs = new Array();
    var values = new Array();
    let bits = 0;
    let n, i, t;

    // Check if there's already a winning hand with all cards
    t = this.getValue();

    if (t >= 8 || t == 4 || t == 5 || t == 6)
    {
      return 0x1f;
    }

    // Check if 4 cards are the same suit (almost a flush)
    for (n = 0; n < 4; n++) { almost_flush[n] = 0; }

    for (n = 0; n < 5; n++)
    {
      almost_flush[Math.floor(this.cards[n] / 13)]++;
    }

    for (n = 0; n < 4; n++)
    {
      if (almost_flush[n] == 4)
      {
        for (i = 0; i < 5; i++)
        {
          if (Math.floor(this.cards[i] / 13) == n) { bits |= (1 << i); }
        }

        return bits;
      }
    }

    // Check for any pairs
    for (n = 0; n < 13; n++) { pairs[n] = -1; }

    for (n = 0; n < 5; n++)
    {
      t = this.cards[n] % 13;

      if (pairs[t] == -1)
      {
        pairs[t] = n;
      }
      else
      {
        bits |= 1 << pairs[t];
        bits |= 1 << n;
      }
    }

    if (bits != 0) { return bits; }

    // Check for a straight (this code can fail if there are pairs)
    let min = -1;
    let max = -1;

    for (n = 0; n < 5; n++)
    {
      values[n] = this.cards[n] % 13;
      //console.log(n + "> " + (this.sorted[n] % 13) + " " + this.sorted[n])
    }

    values.sort(compare);

    if (values[0] + 3 == values[3])
    {
      min = values[0];
      max = min + 4;
    }
    else if (values[1] + 3 == values[4])
    {
      min = values[1];
      max = min + 4;
    }
    if (values[0] + 4 == values[3])
    {
      min = values[0];
      max = min + 4;
    }
    else if (values[1] + 4 == values[4])
    {
      min = values[1];
      max = min + 4;
    }

    if (min != -1)
    {
      for (n = 0; n < 5; n++)
      {
        t = this.cards[n] % 13;
        if (t >= min && t <= max) { bits |= (1 << n); }
      }

      return bits;
    }

    return 0;
  }

  remove(bits)
  {
    let n;

    for (n = 4; n >= 0; n--)
    {
      if ((bits & (1 << n)) != 0)
      {
        this.remove_list.push(n);
      }
    }
  }

  getNextRemove()
  {
    let t;

    if (this.remove_list.length == 0) { return -1; }

    t = this.remove_list.pop();
    this.add_list.unshift(t);

    return t;
  }

  getNextAdd()
  {
    let t;

    if (this.add_list.length == 0) { return -1; }

    t = this.add_list.pop();
    this.showing[t] = true;

    return t;
  }

}

