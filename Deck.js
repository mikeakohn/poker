/**
 *  Poker Game
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2017 by Michael Kohn
 *
 */

class Deck
{
  constructor()
  {
    this.cards = new Array();
    this.temp = new Array();
    this.next = 0;
  }

  shuffle()
  {
    let n;

    for (n = 0; n < 52; n++)
    {
      this.temp[n] = 0;
    }

    for (n = 0; n < 20; n++)
    {
      let card = Math.floor(Math.random() * 52);

      while(this.temp[card] == 1)
      {
        card += 1
        if (card == 52) { card = 0; }
      }

      this.temp[card] = 1;
      this.cards[n] = card;
    }

    this.next = 0;
  }

  getCard()
  {
    if (this.next == 20) { return -1; }

    return this.cards[this.next++];
  }

  getCardFilename(index)
  {
    if (index == -1) { return "cards/card_back.jpeg" }

    let value = index % 13;
    let suit = Math.floor(index / 13);

    let suits = [ "d", "h", "s", "c" ];
    let name = suits[suit];

    if (value < 9) { name = (value + 2) + name; }
    else if (value == 9) { name = "j" + name; }
    else if (value == 10) { name = "q" + name; }
    else if (value == 11) { name = "k" + name; }
    else if (value == 12) { name = "a" + name; }

    let filename = "cards/card_" + name + ".jpeg";

    //console.log(index + " " + value + " " + suit + " " + name + " " + filename);

    return filename;
  }

  dump()
  {
    for (let n = 0; n < 15; n++)
    {
      console.log(this.cards[n]);
    }
  }
}

