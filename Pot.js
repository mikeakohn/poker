/**
 *  Poker Game
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2017 by Michael Kohn
 *
 */

class Pot
{
  constructor()
  {
    this.money = 0;
  }

  addMoney(value)
  {
    this.money += value;
  }

  getMoney()
  {
    return this.money;
  }

  empty()
  {
    let money = this.money;
    this.money = 0;
    return money;
  }
}

