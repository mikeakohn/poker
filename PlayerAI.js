/**
 *  Poker Game
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2017 by Michael Kohn
 *
 */

class PlayerAI extends Player
{
  constructor()
  {
    super();

    // Odds of bluffing are 50%
    this.bluff = 50;

    // Trust in the opponent is 50%
    this.trust = 50;
  }

  getBet(value, suggest)
  {
    // If there is no hand, don't bet (or bluff)
    if (suggest == 0)
    {
      return 0;
    }

    if (value > 0)
    {
      // FIXME - Add bluff.
      return 3;
    }

    // Could bet based on actual hand, but for now just
    // bet based on suggestion of cards to keep.
    let count = 0

    for (let i = 0; i < 5; i++)
    {
      // Could use a lookup table, but who cares how slow this is.
      if ((suggest & 1) == 1) { count++; }
      suggest = suggest >> 1;
    }

    // This should be almost flush or straight.
    if (count == 4)
    {
      return 2;
    }

    // This is probably a pair
    return 2;
  }

  computeBet()
  {
    let suggest = this.hand.suggest();
    let value = this.hand.getValue();

    let bet = this.getBet(value, suggest);

    return bet;
  }

  shouldFold(value)
  {
    if (Math.floor(Math.random() * 100) < this.bluff)
    {
      return true;
    }

    return false;
  }

  raise(money)
  {
    let suggest = this.hand.suggest();
    let value = this.hand.getValue();

console.log(value);
    if (value == 0)
    {
      return 0;
    }

    let diff = money - this.current_bet;

    this.current_bet = money;

    return diff;
  }
}

