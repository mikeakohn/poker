/**
 *  Poker Game
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2017 by Michael Kohn
 *
 */

class Player
{
  constructor()
  {
    this.reset();
    this.hand = new Hand();
  }

  reset()
  {
    this.money = 10;
    this.state = 0;
    this.current_bet = 0;
    this.owe = 0;
  }

  getMoney()
  {
    return this.money - this.owe * 10;
  }

  addMoney(money)
  {
    this.money += money;
  }

  resetCurrentBet()
  {
    this.current_bet = 0;
  }

  getCurrentBet()
  {
    return this.current_bet;
  }

  betMoney(value)
  {
    this.current_bet = value;
    this.money -= value;

    while(this.money < 0)
    {
      this.owe++;
      this.money += 10;
    }
  }

  checkOwe()
  {
    if (this.owe == 0) { return false; }

    this.owe--;
    this.state++;

    return true;
  }

  getState()
  {
    return this.state;
  }

  buyBack()
  {
    if (this.state == 0) { return false; }
    if (this.money < 10) { return false; }

    while(this.state != 0 && this.money >= 10)
    {
      this.money -= 10;
      this.state--;
    }

    return true;
  }
}

